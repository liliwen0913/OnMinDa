package com.newthread.android.ui.fangDao;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.widget.*;
import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.newthread.android.R;
import com.newthread.android.activity.main.BaseSFActivity;
import com.newthread.android.activity.main.MyApplication;
import com.newthread.android.camera.FangDaoService;
import com.newthread.android.mail.MailSender;
import com.newthread.android.manager.MailManager;
import com.newthread.android.util.AndroidUtil;
import com.newthread.android.util.FileUtil;
import com.newthread.android.util.MyPreferenceManager;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by jindongping on 14-9-17.
 */
public class FangDaoActivity extends BaseSFActivity {
    private String mail, passWd;
    private ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fangdao);
        inIt();
    }

    private final Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case 1:
                    progressBar.setVisibility(View.VISIBLE);
                    break;
                case 2:
                    progressBar.setVisibility(View.GONE);
                    break;
                case 3:
                    Toast.makeText(getApplicationContext(), "发送成功", Toast.LENGTH_SHORT).show();
                    break;
                case 4:
                    Toast.makeText(getApplicationContext(), "发送失败,网络不好或者密码错误,请退出应用后重试", Toast.LENGTH_LONG).show();
                    break;
                case 5:
                    Toast.makeText(getApplicationContext(), "不支持该邮箱,请更换邮箱", Toast.LENGTH_LONG).show();
                    break;
                default:
                    progressBar.setVisibility(View.GONE);
            }
        }
    };

    private void inIt() {
        ActionBar ab = getSupportActionBar();
        ab.setDisplayUseLogoEnabled(false);
        ab.setDisplayHomeAsUpEnabled(true);
        ab.setHomeButtonEnabled(true);
        ab.setDisplayShowHomeEnabled(false);
        ab.setTitle("防盗设置");
        MyPreferenceManager.init(getApplicationContext());
        final EditText mailEt = (EditText) this.findViewById(R.id.course_login_account);
        final EditText mailPwEt = (EditText) this.findViewById(R.id.course_login_password);
        final CheckBox checkBox = (CheckBox) this.findViewById(R.id.course_login_checkbox);
        String tMail = MyPreferenceManager.getString("mail", "");
        String tPwd = MyPreferenceManager.getString("mailPwd", "");
        if (!tMail.equals("") && !tPwd.equals("")) {
            mailEt.setText(tMail);
            mailPwEt.setText(tPwd);
        }
        progressBar = (ProgressBar) this.findViewById(R.id.library_list_loading);
        TextView ok = (TextView) this.findViewById(R.id.course_login_btn);
        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isMailAddress(mailEt.getText().toString())) {
                    passWd = mailPwEt.getText().toString();
                    mail = mailEt.getText().toString();
                    if (!checkBox.isChecked()) {
                        MyPreferenceManager.commitString("mail", "");
                        MyPreferenceManager.commitString("mailPwd", "");
                    }
                    MailManager.getInstance().sendEmail(mail, passWd, mail, "人在民大防盗邮件", "text", null, new MailSender.CallBack() {
                        @Override
                        public void sendState(String state) {
                            switch (state) {
                                case "sending":
                                    handler.sendEmptyMessage(1);
                                    break;

                                case "sended":
                                    handler.sendEmptyMessage(2);
                                    handler.sendEmptyMessage(3);
                                    if (checkBox.isChecked()) {
                                        MyPreferenceManager.commitString("mail", mail);
                                        MyPreferenceManager.commitString("mailPwd", passWd);
                                    }
                                    MyPreferenceManager.commitString("telNumber", AndroidUtil.getSimSerialNumber(getApplicationContext()));
                                    break;
                                case "send fail":
                                    handler.sendEmptyMessage(2);
                                    handler.sendEmptyMessage(4);
                                    break;
                                case "no host":
                                    handler.sendEmptyMessage(2);
                                    handler.sendEmptyMessage(5);
                                default:
                                    handler.sendEmptyMessage(2);
                                    handler.sendEmptyMessage(4);
                            }
                        }
                    });
                } else {
                    Toast.makeText(getApplicationContext(), "您输入的邮箱有误", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private boolean isMailAddress(String mailAddress) {
        Pattern p = Pattern.compile("^([a-zA-Z0-9_-])+@([a-zA-Z0-9_-])+(\\.([a-zA-Z0-9_-])+)+$");
        Matcher m = p.matcher(mailAddress);
        return m.matches();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        menu.add("开启防盗服务").setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);
        return true;
    }

    @Override
    public boolean onMenuItemSelected(int featureId, MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                overridePendingTransition(R.anim.in_from_left, R.anim.out_to_right);
                break;

        }
        if (item.getTitle().equals("开启防盗服务")) {
            if (!MyPreferenceManager.getString("mail", "").equals("")) {
                Intent intent = new Intent(getApplicationContext(), FangDaoService.class);
                startService(intent);
                Toast.makeText(getApplicationContext(), "开启成功", Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(getApplicationContext(), "需要先测试发送成功", Toast.LENGTH_LONG).show();
            }
        }
        return super.onMenuItemSelected(featureId, item);

    }

}
