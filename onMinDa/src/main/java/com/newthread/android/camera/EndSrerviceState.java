package com.newthread.android.camera;

public class EndSrerviceState extends ServiceState{

	static class CloseSrerviceStateHolder{
		static EndSrerviceState instance = new EndSrerviceState();
	}
	public static EndSrerviceState getInstance(){
		return CloseSrerviceStateHolder.instance;
	}
	public EndSrerviceState() {
		this.setIsaLive(false);
	}
	
}
