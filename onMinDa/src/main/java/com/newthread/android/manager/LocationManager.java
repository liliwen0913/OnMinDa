package com.newthread.android.manager;

import android.content.Context;
import com.baidu.location.BDLocationListener;
import com.baidu.location.LocationClient;
import com.baidu.location.LocationClientOption;

/**
 * Created by jindongping on 14-9-18.
 */
public class LocationManager {
    private LocationClient mLocationClient;
    private static LocationManager instance;

    public static LocationManager getInstance(Context context) {
        synchronized (MailManager.class) {
            if (instance == null) {
                instance = new LocationManager(context);
            }
        }
        return instance;
    }

    private LocationManager(Context context) {
        this.mLocationClient = new LocationClient(context);
        mLocationClient.setLocOption(getSimpleLocOption());
    }

    private LocationClientOption getSimpleLocOption() {
        LocationClientOption option = new LocationClientOption();
        option.setLocationMode(LocationClientOption.LocationMode.Hight_Accuracy);//设置定位模式
        option.setCoorType("gcj02");//返回的定位结果是百度经纬度，默认值gcj02
        option.setScanSpan(5000);//设置发起定位请求的间隔时间为5000ms
        option.setIsNeedAddress(true);
        return option;
    }

    /**
     * 必须设置定位回调接口，否则无法发起网络请求。
     * @param bdLocationListener － 回调接口
     */
    public void setBdLocationListener(BDLocationListener bdLocationListener){
        mLocationClient.registerLocationListener(bdLocationListener);
    }
    public void start(){
        mLocationClient.start();
    }
    public void stop(){
        mLocationClient.stop();
    }
}
