package com.newthread.android.mail;


/**
 * Created by 翌日黄昏 on 13-11-21.
 */
public abstract class AutoMailSetting {

    public abstract  MailSetting getMailSetting(String address);

    protected MailSetting getQqMailSetting() {
        MailSetting mailSetting = new MailSetting("smtp.qq.com", "25");
        return mailSetting;
    }
    protected MailSetting getGMailSetting() {
        MailSetting mailSetting = new MailSetting("smtp.gmail.com", "25");
        return mailSetting;
    }
    protected MailSetting get126MailSetting() {
        MailSetting mailSetting = new MailSetting("smtp.126.com", "25");
        return mailSetting;
    }
    protected MailSetting get163MailSetting() {
        MailSetting mailSetting = new MailSetting("smtp.163.com", "25");
        return mailSetting;
    }
}
