package com.newthread.android.mail;

import javax.mail.Authenticator;
import javax.mail.PasswordAuthentication;

/**
 * Created by 翌日黄昏 on 13-11-15.
 */
public class MyAuthenticator extends Authenticator {
    String userName=null;
    String password=null;
    public MyAuthenticator(){
    }
    public MyAuthenticator(String username, String password) {
        this.userName = username;
        this.password = password;
    }
    protected PasswordAuthentication getPasswordAuthentication(){
        return new PasswordAuthentication(userName, password);
    }

}
