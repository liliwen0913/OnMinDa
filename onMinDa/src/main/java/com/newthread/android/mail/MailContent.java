package com.newthread.android.mail;

/**
 * Created by 翌日黄昏 on 13-11-20.
 */
public class MailContent {
    private String subject;
    private String content;
    private String attachmentfinal;

    public MailContent() {

    }

    public MailContent(String subject, String content, String attachmentfinal) {
        this.subject = subject;
        this.content = content;
        this.attachmentfinal = attachmentfinal;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public void setAttachmentfinal(String attachmentfinal) {
        this.attachmentfinal = attachmentfinal;
    }

    public String getAttachmentfinal() {
        return attachmentfinal;
    }
}
