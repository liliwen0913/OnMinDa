package com.newthread.android.mail;

/**
 * Created by 翌日黄昏 on 13-11-20.
 */
public class MailSetting {
    private String host;
    private String port;

    public MailSetting() {
        this.host = "";
        this.port = "";
    }

    public MailSetting(String host, String port) {
        this.host = host;
        this.port = port;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public String getPort() {
        return port;
    }

    public void setPort(String port) {
        this.port = port;
    }
}
