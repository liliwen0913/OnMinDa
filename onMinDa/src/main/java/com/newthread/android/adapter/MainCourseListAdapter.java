package com.newthread.android.adapter;

import java.util.ArrayList;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.newthread.android.R;
import com.newthread.android.R.color;
import com.newthread.android.bean.SingleCourseInfo;
import com.newthread.android.util.TimeUtil;

public class MainCourseListAdapter extends BaseAdapter {
    private Context context;
	private ArrayList<SingleCourseInfo> list;
	private LayoutInflater inflater;
    private boolean isToday = false;

	public MainCourseListAdapter(Context con,ArrayList<SingleCourseInfo> courseList) {
        this.context = con;
        this.inflater = LayoutInflater.from(con);
        this.list = courseList;
        this.isToday = false;
    }

    public MainCourseListAdapter(Context con,ArrayList<SingleCourseInfo> courseList, boolean isToday) {
        this.context = con;
        this.inflater = LayoutInflater.from(con);
        this.list = courseList;
        this.isToday = isToday;
    }

	@Override
	public int getCount() {
		return list.size();
	}

	@Override
	public Object getItem(int position) {
		return list.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder = null;

		SingleCourseInfo info = list.get(position);
		if (convertView == null) {
			convertView = inflater.inflate(R.layout.view_main_course_list_item,
					null);

			holder = new ViewHolder();
            holder.item = (RelativeLayout) convertView
                    .findViewById(R.id.item);
			holder.courseName = (TextView) convertView
					.findViewById(R.id.course_name);
			holder.teacher = (TextView) convertView.findViewById(R.id.teacher);
			holder.place = (TextView) convertView.findViewById(R.id.place);
			holder.courseTime = (TextView) convertView
					.findViewById(R.id.course_time);
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}

        holder.courseTime.setText("第" + (position + 1)  + "节");
        if (info.isHaveCourse()) {
            holder.courseName.setText(info.getCourseName());
            holder.place.setText(info.getClassromNum());
        } else {
            holder.courseName.setText("无课");
            holder.place.setText("");
//			int temp_int=(position+1)*2-1;
//			 String temp="第"+temp_int+"-"+(temp_int+1)+"节";
//			holder.courseTime.setText(temp);
        }
        Log.v("000000", info.getNumOfWeek() + "");
        if(isToday) {
            if (position == getCurcourse()) {
                holder.item.setBackgroundColor(context.getResources().getColor(R.color.or));
            } else {
                holder.item.setBackgroundColor(context.getResources().getColor(color.transparent));
            }
        }
		return convertView;
		// SingleCourseInfo info = list.get(position);
		// if (info.isHaveCourse()) {
		// convertView = inflater.inflate(R.layout.view_main_course_list_item,
		// null);
		//
		// TextView courseName = (TextView) convertView
		// .findViewById(R.id.course_name);
		// // TextView teacher = (TextView) convertView
		// // .findViewById(R.id.teacher);
		// TextView place = (TextView) convertView.findViewById(R.id.place);
		// TextView courseTime = (TextView) convertView
		// .findViewById(R.id.course_time);
		//
		// courseName.setText(info.getCourseName());
		// place.setText(info.getClassromNum());
		// courseTime.setText(info.getNumOfDay());
		// } else {
		// convertView = inflater.inflate(
		// R.layout.view_main_course_list_item1, null);
		// }
		//
		// return convertView;
	}

	public class ViewHolder {
        private RelativeLayout item;
		private TextView courseName;
		private TextView teacher;
		private TextView place;
		private TextView courseTime;
	}

    private int getCurcourse() {
        String timeStr = TimeUtil.getHourAndMin();
        int index = 0;
        int time = Integer.parseInt(timeStr);
        //第一节
        if(time > 740) {
            index = 0;
        }
        if(time > 845) {
            index = 1;
        }
        if(time > 940) {
            index = 2;
        }
        if(time > 1045) {
            index = 3;
        }
        //第五节
        if(time > 1140) {
            index = 4;
        }
        if(time > 1455) {
            index = 5;
        }
        if(time > 1550) {
            index = 6;
        }
        if(time > 1645) {
            index = 7;
        }
        //第9节
        if(time > 1740) {
            index = 8;
        }
        if(time > 1920) {
            index = 9;
        }
        if(time > 2015) {
            index = 10;
        }
        if(time > 2110) {
            index = -1;
        }
        if(time < 740) {
            index = -1;
        }
        return index;
    }

}
