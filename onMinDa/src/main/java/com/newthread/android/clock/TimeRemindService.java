package com.newthread.android.clock;

import android.app.Service;
import android.content.Intent;
import android.os.Bundle;
import android.os.IBinder;

/**
 * Created by jindongping on 14-10-13.
 */
public class TimeRemindService extends Service {
    private boolean isFistStarted = true;

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        flags = START_STICKY;
        dealCourseRequest(intent);
        return super.onStartCommand(intent, flags, startId);
    }

    private void dealCourseRequest(Intent intent) {
        if (intent != null && Intent.ACTION_BOOT_COMPLETED.equals(intent.getAction())||isFistStarted) {
            TimeManager.getInstance(getApplicationContext()).delPreTimeTask();
            TimeManager.getInstance(getApplicationContext()).registAllClock();
        }
        if (intent != null && intent.getExtras() != null) {
            Bundle bundle = intent.getExtras();
            TimeTask timeTask = new TimeTask(bundle.getString("time"), "couserBroadcast", "broadcast");
            TimeManager.getInstance(getApplicationContext()).registClock(timeTask, bundle);
            TimeManager.getInstance(getApplicationContext()).addTimeTaskInDB(timeTask);
        }
    }

}
