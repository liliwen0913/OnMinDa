package com.newthread.android.activity.main;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import android.support.v4.app.FragmentActivity;
import android.view.KeyEvent;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.Toast;
import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.newthread.android.R;
import com.newthread.android.util.UpdateManager;
import com.newthread.android.view.DragLayout;
import com.nineoldandroids.view.ViewHelper;

public class OnCampusActivity extends FragmentActivity {
    private DragLayout dl;
    private ListView lv;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_oncampus);
        initView(savedInstanceState);

        // 检查更新
        new UpdateManager(OnCampusActivity.this).checkUpdate();

    }

    List<SlidingMenuConfig.ItemMenu> itemMenus;

    public void initView(Bundle savedInstanceState) {
        dl = (DragLayout) findViewById(R.id.dl);
        final MainContentFragment fragment = new MainContentFragment(getApplicationContext(), dl);
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.on_campus_main_frame, fragment)
                .commit();
        dl.setDragListener(new DragLayout.DragListener() {
            @Override
            public void onOpen() {
                YoYo.with(Techniques.Shake)
                        .duration(2000)
                        .playOn(findViewById(R.id.left_menu_top_view));
            }

            @Override
            public void onClose() {

            }

            @Override
            public void onDrag(float percent) {
                ViewHelper.setAlpha(fragment.getLeftButton(), 1 - percent);
            }
        });

        itemMenus = SlidingMenuConfig.getInstance().getTotalList();
        List<HashMap<String, Object>> data = new ArrayList<>();
        for (SlidingMenuConfig.ItemMenu itemMenu : itemMenus) {
            HashMap<String, Object> map = new HashMap<String, Object>();
            map.put("title", itemMenu.getTitle());
            data.add(map);
        }
        SimpleAdapter adapter = new SimpleAdapter(getApplicationContext(), data, R.layout.view_right_menu_item, new String[]{"title"}, new int[]{R.id.right_menu_list_title});
        lv = (ListView) findViewById(R.id.lv);
        lv.setAdapter(adapter);
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent _intent = new Intent(getApplicationContext(), itemMenus.get(position).getClazz());
                startActivity(_intent);
            }
        });
        findViewById(R.id.setting).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent _intent = new Intent(getApplicationContext(), SettingActivity.class);
                startActivity(_intent);
            }
        });

    }

    private boolean isExit = false;

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        dl.close();
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            exit();
            return false;
        } else {
            return super.onKeyDown(keyCode, event);
        }
    }

    public void exit() {
        if (!isExit) {
            isExit = true;
            Toast.makeText(getApplicationContext(), getString(R.string.exit_hint), Toast.LENGTH_SHORT).show();
            exitHandler.sendEmptyMessageDelayed(0, 2000);
        } else {
            Intent intent = new Intent(Intent.ACTION_MAIN);
            intent.addCategory(Intent.CATEGORY_HOME);
            startActivity(intent);

            System.exit(0);
        }
    }

    final Handler exitHandler = new Handler() {
        public void handleMessage(android.os.Message msg) {
            super.handleMessage(msg);
            isExit = false;
        }
    };
}
